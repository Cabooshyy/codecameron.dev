;;; build-site.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Cameron Miller
;;
;; Author: Cameron Miller <https://github.com/cabooshy>
;; Maintainer: Cameron Miller <cameron@codecameron.dev>
;; Created: January 22, 2022
;; Modified: January 22, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/cabooshy/build-site
;; Package-Requires: ((emacs "24.4"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:
(require 'ox-publish)

;; Define the Publishing Project
(setq org-publish-project-alist
      (list
       (list "codecameron.dev"
             :recursive t
             :base-directory "./content"
             :publishing-directory "./public"
             :publishing-function 'org-html-publish-to-html)))
;; Generate Site Output
(org-publish-all t)

(message "Build Completed!")

(provide 'build-site)
;;; build-site.el ends here
